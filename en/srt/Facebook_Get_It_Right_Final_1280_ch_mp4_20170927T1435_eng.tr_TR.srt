﻿1
00:00:03,222 --> 00:00:05,804
Her gün 1 milyarı aşkın insan aileleriyle, arkadaşlarıyla

2
00:00:05,804 --> 00:00:09,062
ve işletmelerle bağlantı kurmak için Facebook'u kullanıyor.

3
00:00:09,062 --> 00:00:11,421
Facebook, insanların kendilerine özel alanı.

4
00:00:11,421 --> 00:00:12,766
Biz de Facebook'u güvenilir ve yüksek kaliteli

5
00:00:12,766 --> 00:00:15,429
bir platform haline getirmek istiyoruz.

6
00:00:15,429 --> 00:00:19,247
Bu amaçla Facebook, tüm faaliyetlerin kullanıcı beklentilerini

7
00:00:19,247 --> 00:00:23,015
karşıladığından emin olmak için kapsamlı bir reklam yayın ilkesi geliştirdi.

8
00:00:23,015 --> 00:00:24,203
Çünkü reklamlarda uygun

9
00:00:24,203 --> 00:00:27,531
resimler ve yazılar kullanmak

10
00:00:27,531 --> 00:00:29,484
işletmeler için çeşitli avantajlar sağlar.

11
00:00:29,484 --> 00:00:30,509
Uygun olmayan içeriklere sahip

12
00:00:30,509 --> 00:00:33,023
reklamlar ise tam ters etki yaratır.

13
00:00:33,023 --> 00:00:35,234
Bunu bir örnekle ele alalım.

14
00:00:35,234 --> 00:00:38,906
Bu e-ticaret işletmesi, ürün satışlarını artırmak için

15
00:00:38,906 --> 00:00:41,132
internet sitesine daha fazla kişi çekmek istiyor.

16
00:00:41,132 --> 00:00:42,429
Sitesinin çekiciliğini artırmak için

17
00:00:42,429 --> 00:00:43,590
internet domaininde diğer ünlü

18
00:00:43,590 --> 00:00:46,277
markaların adlarına yer veriyor ve

19
00:00:46,277 --> 00:00:49,398
tanıtımlarında abartı ifadeler kullanıyor.

20
00:00:49,398 --> 00:00:52,585
Ayrıca markasız çantalarına marka logoları ekliyor.

21
00:00:52,585 --> 00:00:56,225
Böylece reklamlarının daha ilgi çekici olacağını düşünüyor.

22
00:00:56,225 --> 00:00:59,187
Ama bunun tam tersi oluyor.

23
00:00:59,187 --> 00:01:00,179
Facebook reklam ilkesini

24
00:01:00,179 --> 00:01:03,125
ihlal etmekle kalmayıp, alıcıların ürünlerine

25
00:01:03,125 --> 00:01:06,399
duyduğu güveni de kaybediyor.

26
00:01:06,399 --> 00:01:07,299
Bu kayıp, elde ettiği kazançları geride bırakıyor.

27
00:01:08,497 --> 00:01:09,414
Benzer şekilde,

28
00:01:09,414 --> 00:01:11,875
bir uygulama geliştiricisiyseniz,

29
00:01:11,875 --> 00:01:13,127
reklamlarınızda uygulamanın gerçek işlevlerini

30
00:01:13,127 --> 00:01:15,898
sergilediğinizden emin olun ve gerçek dışı

31
00:01:15,898 --> 00:01:18,492
"öncesi-sonrası" karşılaştırmalarından veya sunulmayan

32
00:01:18,492 --> 00:01:21,382
etki veya özellikleri göstermekten kaçının.

33
00:01:21,382 --> 00:01:23,304
Bu kötü uygulamalar, işletmeniz ile kullanıcılar

34
00:01:23,304 --> 00:01:26,781
arasındaki karşılıklı güven ilişkisini zedeler.

35
00:01:26,781 --> 00:01:27,924
Bir örneğe daha bakalım.

36
00:01:27,924 --> 00:01:30,085
Mayo ve iç giyim ürünleri satan bir internet mağazası düşünelim.

37
00:01:30,085 --> 00:01:32,585
Mağaza reklamlarında bu resimleri kullanıyor.

38
00:01:32,585 --> 00:01:35,218
Hedef kitlede olumsuz bir izlenim bırakmakla kalmayıp

39
00:01:35,218 --> 00:01:38,718
Facebook'un reklam yayınlama ilkesini de ihlal ediyor.

40
00:01:39,976 --> 00:01:41,632
Reklamlarda aşırı müstehcen fotoğraflar kullanılamaz,

41
00:01:41,632 --> 00:01:44,398
ayrıca reklamlar şok edici olamaz,

42
00:01:44,398 --> 00:01:45,390
şiddet içerikleri kullanamaz ve

43
00:01:45,390 --> 00:01:47,781
saygısız içerikler bulunduramaz.

44
00:01:47,781 --> 00:01:50,398
Özellikle de oyun uygulamalarındaki reklamlar.

45
00:01:50,398 --> 00:01:51,406
İzlenecek en iyi uygulama;

46
00:01:51,406 --> 00:01:54,929
ürünün avantajlarını ve benzersiz özelliklerini öne çıkarmaya odaklanmaktır.

47
00:01:54,929 --> 00:01:57,796
Alıcıları yanıltan abartılı veya gerçek olmayan

48
00:01:57,796 --> 00:02:00,070
ifadelerin kullanılmasına kesinlikle izin verilmez.

49
00:02:01,453 --> 00:02:03,031
Aslında Facebook'ta

50
00:02:03,031 --> 00:02:04,304
ürün ve hizmetlerinizin reklamını yapmanın

51
00:02:04,304 --> 00:02:06,695
çeşitli yolları vardır.

52
00:02:06,695 --> 00:02:08,632
Yaratıcı olduğunuz, doğru içerikleri seçtiğiniz ve

53
00:02:08,632 --> 00:02:10,468
yüksek kaliteli reklamlar oluşturduğunuz sürece

54
00:02:10,468 --> 00:02:11,531
iyi bir marka imajı oluşturabilir,

55
00:02:11,531 --> 00:02:13,507
olumlu ve kalıcı bir itibar kazanabilir,

56
00:02:13,507 --> 00:02:15,445
müşterilerinizin güvenini artırabilir,

57
00:02:15,445 --> 00:02:17,414
Facebook'ta reklamlarınızın alaka düzeyi

58
00:02:17,414 --> 00:02:20,335
puanlarını yükseltebilir, açık artırma kazanma

59
00:02:20,335 --> 00:02:24,125
şansınızı artırabilir, teklif ücretlerini düşürebilir ve

60
00:02:24,125 --> 00:02:26,359
sağlıklı bir alışveriş döngüsü oluşturabilirsiniz.

61
00:02:26,359 --> 00:02:27,656
İyi bir reklam

62
00:02:27,656 --> 00:02:30,484
insanların akışta aşağı inerken durmasını sağlar

63
00:02:30,484 --> 00:02:31,695
ve onların kalplerine dokunur.

64
00:02:31,695 --> 00:02:34,085
Böylece daha fazla iş fırsatı elde edebilir ve

65
00:02:34,085 --> 00:02:36,125
müşterilerinize karşı dürüstlüğünüzü koruyabilirsiniz.

66
00:02:36,125 --> 00:02:37,968
Ürünlerinizin hikayesini paylaşmanız

67
00:02:37,968 --> 00:02:40,664
insanların markanızı sevmesini sağlar.

68
00:02:42,867 --> 00:02:43,812
Çünkü

69
00:02:43,812 --> 00:02:46,308
başarılı olmanın yolu doğru şeyleri yapmaktan geçer.

